package ictgradschool.industry.arrays.printpattern;

public class Pattern {
    private int number;
    private char symbols;

    public Pattern(int number, char symbols) {
        this.number = number;
        this.symbols = symbols;

    }
    public String toString(){
        String pattern = "";
        for(int i = 0; i < number; i ++){
            pattern += symbols;
        }
        return pattern;

    }

    public void setNumberOfCharacters(int num){
        this.number = num;

    }

    public int getNumberOfCharacters(){
        return number;
    }

}
